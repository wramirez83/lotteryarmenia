# piaf-vue
## Version NVM 
v13.14.0
## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## Helper
- https://www.npmjs.com/package/vue-search-select

### Screenshoots
<img src="/screenshoots/Loterias.png"/>
<img src="/screenshoots/Listaloterias.png"/>
<img src="/screenshoots/ListadoVentas.png"/>
<img src="/screenshoots/limites.png"/>
<img src="/screenshoots/compartir.png"/>

### Avatar
https://www.npmjs.com/package/vue-avatar

### Icon PWA
https://cthedot.de/icongen/#output

### Calendarios
https://vcalendar.io/examples/datepickers.html

### GMAIL
Usuario: soporteecuador3@gmail.com
Clave: EstudienVagos

npm i compressorjs






