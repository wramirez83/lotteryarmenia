const envDev = 'bonaires';

const env = {
    local: {
        url: 'http://127.0.0.1:8000/api/v1',
        defaultColor: 'light.orangecarrot',
        background: 'fixed-background-llamadoz',
        side: 'image-side',
        name: 'local'
    },
    bonaires:{
        url: 'https://api.ponchado.online/api/v1',
        defaultColor: 'light.orangecarrot',
        background: 'fixed-background-llamadoz',
        side: 'image-side',
        name: 'bonaires'
    },
    env:{
      url: process.env.VUE_APP_URL,
      defaultColor: process.env.VUE_APP_DEFAULTCOLOR,
      background: process.env.VUE_APP_BACKGROUND,
      side: process.env.VUE_APP_SIDE,
      name: process.env.VUE_APP_APP_NAME,
  },


};
export const versionApp = '2.7.0';
export const defaultMenuType = 'menu-default' // 'menu-default', 'menu-sub-hidden', 'menu-hidden';
export const adminRoot = '/app';
export const searchPath = `${adminRoot}/pages/miscellaneous/search`
export const buyUrl = 'https://1.envato.market/nEyZa'
export const timeClose = 20;
export const dataEnv = env[envDev];
export const apiUrl = env[envDev].url;

export const subHiddenBreakpoint = 1440
export const menuHiddenBreakpoint = 768

export const defaultLocale = 'es'
export const defaultDirection = 'ltr'
export const localeOptions = [
    { id: 'en', name: 'English LTR', direction: 'ltr' },
    { id: 'es', name: 'Español', direction: 'ltr' },
    { id: 'enrtl', name: 'English RTL', direction: 'rtl' }
]

export const firebaseConfig = {
    apiKey: "AIzaSyDe94G7L-3soXVSpVbsYlB5DfYH2L91aTU",
    authDomain: "piaf-vue-login.firebaseapp.com",
    databaseURL: "https://piaf-vue-login.firebaseio.com",
    projectId: "piaf-vue-login",
    storageBucket: "piaf-vue-login.appspot.com",
    messagingSenderId: "557576321564",
    appId: "1:557576321564:web:bc2ce73477aff5c2197dd9"
};

export const currentUser = {
    id: 0,
    title: '0',
    img: '',
    date: ''
}

export const monthsData = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];

export var isAuthActive = false
export const themeRadiusStorageKey = 'theme_radius'
export const themeSelectedColorStorageKey = 'theme_selected_color'
export const defaultColor = env[envDev].defaultColor;
export const colors = ['bluenavy', 'blueyale', 'blueolympic', 'greenmoss', 'greenlime', 'purplemonster', 'orangecarrot', 'redruby', 'yellowgranola', 'greysteel'];
export const weekDay = [
    { value: 0, text: 'Domingo' },
    { value: 1, text: 'Lunes' },
    { value: 2, text: 'Martes' },
    { value: 3, text: 'Miercoles' },
    { value: 4, text: 'Jueves' },
    { value: 5, text: 'Viernes' },
    { value: 6, text: 'Sabado' }
];
export const weekDayArray = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'];
export const methodStorage = 'local'; //local - session
