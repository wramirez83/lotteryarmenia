import { adminRoot } from "./config";
import { menuAdminLottery } from './menuAdmin';

const dataUser = JSON.parse(localStorage.getItem('user'));
const permission = JSON.parse(localStorage.getItem('permission'));

const data = [{
        id: "dashboards",
        icon: "iconsminds-shop-4",
        label: "Principal",
        role: 'Vendedor',
        to: `${adminRoot}/dashboards/default`,

    },
    {
        id: "resume",
        icon: "iconsminds-library",
        label: "Resumen$",
        role: 'Promotor, Administrador, Observador',
        to: `${adminRoot}/resume/all`,

    },
    {
        //Rifas Raffle
        id: "raffleIdMenu",
        icon: "iconsminds-receipt-4",
        label: "Rifas",
        role: 'Vendedor',
        to: `${adminRoot}/raffle/bet`
    },
    // {
    //     //Rifas Raffle MEGA
    //     id: "megaRaffle",
    //     icon: "iconsminds-check",
    //     label: "MEGA Rifas",
    //     role: 'Vendedor',
    //     to: `${adminRoot}/raffle/mega`
    // },
    {
        id: "lastsale",
        icon: "simple-icon-basket-loaded",
        label: "Mis Ventas",
        role: 'Vendedor',
        to: `${adminRoot}/sales`,
        subs: [{
                icon: "iconsminds-cart-quantity",
                label: "Mis Ventas de Hoy",
                to: `${adminRoot}/sales/last`
            },
            {
                icon: "iconsminds-cash-register-2",
                label: "Ventas Detalladas",
                to: `${adminRoot}/sales/all`
            },
            {
                icon: "iconsminds-24-hour",
                label: "Jugados por Loteria",
                to: `${adminRoot}/sales/lottery`
            },
            {
                icon: "simple-icon-calendar",
                label: "Ventas por Clientes",
                to: `${adminRoot}/sales/listSaleDayOfDay`
            },
            {
                icon: "iconsminds-coins",
                label: "Ventas por Fecha",
                to: `${adminRoot}/sales/listSaleRangeDate`
            },
            {
                icon: "iconsminds-coins",
                label: "Ventas por Fecha MEGA",
                to: `${adminRoot}/sales/listSaleDateMega`
            }
        ]
    },
    {
        id: "myclients",
        icon: "simple-icon-user-following",
        label: "Mis Clientes",
        role: 'Vendedor',
        to: `${adminRoot}/clients/ls`,
    },
    {
        id: "lastsalePromotor",
        icon: "iconsminds-mens",
        label: "Liquidación",
        role: 'Promotor, Administrador, Observador',
        to: `${adminRoot}/sales/user`,
        subs: [
            {
                icon: 'iconsminds-mens',
                label: 'Ventas Detalladas',
                to: `${adminRoot}/closing/today`
            },
            {
                id: 'promotorLiquidacion',
                icon: 'iconsminds-handshake',
                label: 'Ventas Promotor',
                to: `${adminRoot}/closing/prom`
            },
            {
                icon: 'iconsminds-bar-chart-4',
                label: 'Ventas Vendedor',
                to: `${adminRoot}/closing/sellerTwo`
            },
            {
                icon: 'iconsminds-arrow-around',
                label: 'Ventas MegaRifa',
                to: `${adminRoot}/closing/promoMega`
            }
        ]
    },
    {
        id: 'results',
        icon: 'iconsminds-monitor-analytics',
        label: 'Resultados',
        role: 'Vendedor, Promotor, Administrador, Observador',
        to: `${adminRoot}/results/all`
    },
    {
        id: 'winners',
        icon: 'simple-icon-trophy',
        label: 'Premios',
        role: 'Vendedor, Promotor',
        to: `${adminRoot}/winner/all`
    },

];

//lottery,
//promotore,

data.push(menuAdminLottery['promoter']);
data.push(menuAdminLottery['promoterAdministrador']);
data.push(menuAdminLottery['lottery']);
data.push(menuAdminLottery['recharge']);
data.push(menuAdminLottery['scrutiny']);
// Chat of support
data.push({
    id: 'chat',
    icon: 'simple-icon-bubbles',
    label: 'Soporte',
    role: 'Vendedor, Promotor, Administrador, Observador',
    to: `${adminRoot}/chat`
});

export default data;
