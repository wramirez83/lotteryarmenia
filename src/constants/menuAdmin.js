import { adminRoot } from "./config";
const permission = JSON.parse(localStorage.getItem('permission'));
const menuAdminLotteryInt = new Array();
menuAdminLotteryInt['lottery'] = {
    id: "lottery",
    icon: "iconsminds-prater",
    label: "Loterias",
    role: 'Administrador',
    to: `${adminRoot}/lotterys`,
    subs: [{
            icon: "simple-icon-list",
            label: "Consultar Loterias",
            to: `${adminRoot}/lotterys/list`,
        },
        {
            icon: "iconsminds-firewall",
            label: "Limites de Loterias",
            to: `${adminRoot}/lotterys/limit`,
        }
    ]
};

    var subMenuPromVend = [
        {
            icon: 'simple-icon-user-follow',
            label: '',
            to: `${adminRoot}/promotors/new`
        },
        {
            icon: 'simple-icon-people',
            label: 'Consultar Promotor',
            to: `${adminRoot}/promotors/list`
        },
        {
            icon: 'iconsminds-business-man',
            label: 'Consultar Vendedor',
            to: `${adminRoot}/promotors/list/seller`
        }
    ];


menuAdminLotteryInt['promoter'] = {
    id: "promoter",
    icon: "simple-icon-people",
    label: "",
    role: 'Promotor',
    to: `${adminRoot}/promotors/`,
    subs: subMenuPromVend
}

menuAdminLotteryInt['promoterAdministrador'] = {
    id: "promoters",
    icon: "simple-icon-people",
    label: "Prom/Vendedor",
    role: 'Administrador, Observador',
    to: `${adminRoot}/promotors/`,
    subs: [
        {
            icon: 'simple-icon-user-follow',
            label: 'Nuevo Usuario',
            to: `${adminRoot}/promotors/new`
        },
        {
            icon: 'simple-icon-people',
            label: 'Consultar Promotor',
            to: `${adminRoot}/promotors/list`
        },
        {
            icon: 'iconsminds-business-man',
            label: 'Consultar Vendedor',
            to: `${adminRoot}/promotors/list/seller`
        }
    ]
}

menuAdminLotteryInt['recharge'] = {
    id: "recharge",
    icon: "iconsminds-mail-money",
    label: "Recargas",
    role: 'Promotor, Administrador, Observador',
    to: `${adminRoot}/recharge`,
    subs: [{
            icon: "iconsminds-money-bag",
            label: "Recargar Usuario",
            to: `${adminRoot}/recharge/add`,
        },
        {
            icon: "iconsminds-letter-sent",
            label: "Quitar Saldo",
            to: `${adminRoot}/recharge/remove`,
        },
    ]
}

menuAdminLotteryInt['scrutiny'] = {
    id: "scrutiny",
    icon: "simple-icon-settings",
    label: "Administrar",
    role: 'Administrador',
    to: `${adminRoot}/scrutiny`,
    subs: [{
            icon: "iconsminds-down-1",
            label: "Resultados",
            to: `${adminRoot}/scrutiny/values`,
        },
        {
            icon: "iconsminds-check",
            label: "Escrutar",
            to: `${adminRoot}/scrutiny/generate`,
        },
        {
            icon: "simple-icon-calculator",
            label: "Numeros Jugados",
            to: `${adminRoot}/scrutiny/numbersPlayed`,
        },
        {
            icon: "iconsminds-billing",
            label: "Premiación",
            to: `${adminRoot}/scrutiny/award`,
        },
        {
            icon: "simple-icon-calendar",
            label: "Premiación de Bonos",
            to: `${adminRoot}/scrutiny/award/raffle`,
        },
        {
            icon: "iconsminds-add-space-before-paragraph",
            label: "Buscar Colilla Chance",
            to: `${adminRoot}/scrutiny/ticket/lottery`,
        },
        {
            icon: "iconsminds-add-space-after-paragraph",
            label: "Buscar Colilla **Rifa**",
            to: `${adminRoot}/scrutiny/ticket/raffle`,
        },
        {
            icon: "simple-icon-user-following",
            label: "Buscar Clientes",
            to: `${adminRoot}/scrutiny/client/search`,
        },
        // {
        //     icon: "iconsminds-check",
        //     label: "Crear MEGA RIFA",
        //     to: `${adminRoot}/mega`,
        // },
    ]
}
export const menuAdminLottery = menuAdminLotteryInt;
