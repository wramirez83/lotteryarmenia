class doubleDig{
    getDoubleValue(val){
        const number1 = val[0];
        const number2 = val[1];
        const number3 = val[2];

        if((number1 == number2) && (number1 == number3)){
            return 'none';
        }else{
            return 'inline';
        }
    }

    getDoubleValueFour(val){
        const number1 = val[0];
        const number2 = val[1];
        const number3 = val[2];
        const number4 = val[3];

        if((number1 == number2) && (number1 == number3) && (number4 == number2)){
            return 'none';
        }else{
            return 'inline';
        }
    }
}

export default new doubleDig();