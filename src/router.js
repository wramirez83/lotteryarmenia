import Vue from "vue";
import VueRouter from "vue-router";
import AuthRequired from "./utils/AuthRequired";
import { adminRoot } from "./constants/config";

Vue.use(VueRouter);

const routes = [{
        path: "/",
        component: () =>
            import ( /* webpackChunkName: "home" */ "./views/home"),
        redirect: `/user/login`,
    },
    {
        path: adminRoot,
        component: () =>
            import ( /* webpackChunkName: "app" */ "./views/app"),
        redirect: `${adminRoot}/dashboards`,
        beforeEnter: AuthRequired,
        children: [{
                path: "dashboards",
                component: () =>
                    import ( /* webpackChunkName: "dashboards" */ "./views/app/dashboards"),
                redirect: `${adminRoot}/dashboards/default`,
                children: [{
                        path: "default",
                        component: () =>
                            import ( /* webpackChunkName: "dashboards" */ "./views/app/dashboards/Default")
                    },
                    {
                        path: "analytics",
                        component: () =>
                            import ( /* webpackChunkName: "dashboards" */ "./views/app/dashboards/Analytics")
                    },
                    {
                        path: "ecommerce",
                        component: () =>
                            import ( /* webpackChunkName: "dashboards" */ "./views/app/dashboards/Ecommerce")
                    },
                    {
                        path: "content",
                        component: () =>
                            import ( /* webpackChunkName: "dashboards" */ "./views/app/dashboards/Content")
                    }
                ]
            },
            {
                path: 'resume/all',
                component: () =>
                    import ( /* webpackChunkName: "resume" */ "./views/app/resume/all")
            },
            {
                path: 'raffle/bet',
                component: () =>
                    import ( /* webpackChunkName: "raffle" */ "./views/app/raffle/raffle")
            },
            {
                path: 'raffle/mega',
                component: () =>
                    import ( /* webpackChunkName: "raffle" */ "./views/app/mega/BetMega")
            },
            {
                path: "lotterys",
                component: () =>
                    import ( /* webpackChunkName: "lotterys" */ "./views/app/lotterys"),
                redirect: `${adminRoot}/lotterys/list`,
                children: [{
                        path: "list",
                        component: () =>
                            import ( /* webpackChunkName: "lotterys" */ "./views/app/lotterys/lotterylist")
                    },
                    {
                        path: "limit",
                        component: () =>
                            import ( /* webpackChunkName: "lotterys" */ "./views/app/lotterys/lotterylimit")
                    },
                ]
            },
            {
                path: "promotors",
                component: () =>
                    import ( /* webpackChunkName: "promotors" */ "./views/app/promotors"),
                redirect: `${adminRoot}/promotors/list`,
                children: [
                    {
                        path: "list",
                        component: () =>
                        import ( /* webpackChunkName: "promotors" */ "./views/app/promotors/listpromotors")
                    },
                    {
                        path: "list/seller",
                        component: () =>
                        import ( /* webpackChunkName: "promotors" */ "./views/app/promotors/listpromotorsSeller")
                    },
                    {
                        path: "new",
                        component: () =>
                        import ( /* webpackChunkName: "promotors" */ "./views/app/promotors/newpromotors")
                    },
                ]
            },
            {
                path: "sales",
                component: () =>
                    import ( /* webpackChunkName: "sales" */ "./views/app/sales"),
                redirect: `${adminRoot}/sales/last`,
                children: [{
                        path: "last",
                        component: () =>
                            import ( /* webpackChunkName: "sales" */ "./views/app/sales/listsale")
                    },
                    {
                        path: "all",
                        component: () =>
                            import ( /* webpackChunkName: "sales" */ "./views/app/sales/listsaleDate")
                    },
                    {
                        path: "lottery",
                        component: () =>
                            import ( /* webpackChunkName: "sales" */ "./views/app/sales/listSaleLottery")
                    },
                    {
                        path: "user",
                        component: () =>
                            import ( /* webpackChunkName: "sales" */ "./views/app/sales/listSaleUser")
                    },
                    {
                        path: "listSaleDayOfDay",
                        component: () =>
                            import ( /* webpackChunkName: "sales" */ "./views/app/sales/listSaleDayOfDay")
                    },
                    {
                        path: "listSaleRangeDate",
                        component: () =>
                            import ( /* webpackChunkName: "sales" */ "./views/app/sales/lisSaleRangeDate")
                    },
                    {
                        path: "listSaleDateMega",
                        component: () =>
                            import ( /* webpackChunkName: "sales" */ "./views/app/sales/listsaleDateMega")
                    }
                ]
            },
            {
                path: "clients/ls",
                component: () => import ( /* webpackChunkName: "clients"*/ "./views/app/clients/clients")
            },
            {
                path: "results",
                component: () =>
                    import ( /* webpackChunkName: "results" */ "./views/app/results"),
                redirect: `${adminRoot}/results/all`,
                children: [{
                    path: "all",
                    component: () =>
                        import ( /* webpackChunkName: "results") */ "./views/app/results/resultall")
                }]
            },
            {
                path: "winner",
                component: () =>
                    import ( /* webpackChunkName: "results" */ "./views/app/winner"),
                redirect: `${adminRoot}/winner/all`,
                children: [{
                    path: "all",
                    component: () =>
                        import ( /* webpackChunkName: "results") */ "./views/app/winner/all")
                }]
            },
            {
                path: "closing",
                component: () =>
                    import ( /* webpackChunkName: "closing" */ "./views/app/closing"),
                redirect: `${adminRoot}/closing/today`,
                children: [{
                        path: "today",
                        component: () =>
                            import ( /* webpackChunkName: "closing") */ "./views/app/closing/closingall")
                    },
                    {
                        path: "days",
                        component: () =>
                            import ( /* webpackChunkName: "closing") */ "./views/app/closing/closingdays")
                    },
                    {
                        path: "seller",
                        component: () =>
                            import ( /* webpackChunkName: "closing") */ "./views/app/closing/closingSeller")
                    },
                    {
                        path: "prom",
                        component: () =>
                            import ( /* webpackChunkName: "closing") */ "./views/app/closing/closingProm")
                    },
                    {
                        path: "sellerTwo",
                        component: () =>
                            import ( /* webpackChunkName: "closing") */ "./views/app/closing/closingSellerTwo")
                    },
                    {
                        path: "promoMega",
                        component: () =>
                            import ( /* webpackChunkName: "closing") */ "./views/app/closing/closingMegaPromo")
                    },
                ]
            },
            {
                path: "recharge",
                component: () =>
                    import ( /* webpackChunkName: "recharge" */ "./views/app/recharge"),
                redirect: `${adminRoot}/recharge/add`,
                children: [{
                        path: "add",
                        component: () =>
                            import ( /* webpackChunkName: "recharge" */ "./views/app/recharge/rechargeAdd")
                    },
                    {
                        path: "remove",
                        component: () =>
                            import ( /* webpackChunkName: "recharge" */ "./views/app/recharge/rechargeDel")
                    }
                ]
            },
            {
                path: "scrutiny",
                component: () =>
                    import ( /* webpackChName: "scrutiny"*/ "./views/app/scrutiny"),
                redirect: `${adminRoot}/scrutiny`,
                children: [{
                        path: "values",
                        component: () =>
                            import ( /* webpackChunkName: "scrutiny"*/ "./views/app/scrutiny/scrutinyValues")
                    },
                    {
                        path: "generate",
                        component: () =>
                            import ( /* webpackChunkName: "scrutiny"*/ "./views/app/scrutiny/generateScrutiny")
                    },
                    {
                        path: "numbersPlayed",
                        component: () =>
                            import ( /* webpackChunkName: "scrutiny"*/ "./views/app/scrutiny/numbersPlayed")
                    },
                    {
                        path: "award",
                        component: () =>
                            import ( /* webpackChunkName: "scrutiny"*/ "./views/app/award/awardLottery")
                    },
                    {
                        path: "award/raffle",
                        component: () =>
                            import ( /* webpackChunkName: "scrutiny"*/ "./views/app/award/awardRaffle")
                    },
                    {
                        path: "ticket/lottery",
                        component: () =>
                            import ( /* webpackChunkName: "scrutiny"*/ "./views/app/ticket/ticket")
                    },
                    {
                        path: "ticket/raffle",
                        component: () =>
                            import ( /* webpackChunkName: "scrutiny"*/ "./views/app/ticket/raffle")
                    },
                    {
                        path: "client/search",
                        component: () =>
                            import ( /* webpackChunkName: "scrutiny"*/ "./views/app/scrutiny/searchClient")
                    }
                ]
            },
            {
                path: 'chat',
                component: () =>
                    import ( /* webpackChunkName: "Chat"*/ "./views/app/support/chat"),
            },
            {
                path: "ui",
                component: () =>
                    import ( /* webpackChunkName: "ui" */ "./views/app/ui"),
                redirect: `${adminRoot}/ui/forms`,
                children: [{
                        path: "forms",
                        component: () =>
                            import ( /* webpackChunkName : "forms" */ "./views/app/ui/forms"),
                        redirect: `${adminRoot}/ui/forms/layouts`,
                        children: [{
                                path: "layouts",
                                component: () =>
                                    import ( /* webpackChunkName: "forms" */ "./views/app/ui/forms/Layouts")
                            },
                            {
                                path: "components",
                                component: () =>
                                    import ( /* webpackChunkName: "forms" */ "./views/app/ui/forms/Components")
                            },
                            {
                                path: "validations",
                                component: () =>
                                    import ( /* webpackChunkName: "forms" */ "./views/app/ui/forms/Validations")
                            },
                            {
                                path: "wizard",
                                component: () =>
                                    import ( /* webpackChunkName: "forms" */ "./views/app/ui/forms/Wizard")
                            },
                        ]
                    },
                    {
                        path: "datatables",
                        component: () =>
                            import ( /* webpackChunkName : "datatables" */ "./views/app/ui/datatables"),
                        redirect: `${adminRoot}/ui/datatables/divided-table`,
                        children: [{
                                path: "divided-table",
                                component: () =>
                                    import ( /* webpackChunkName: "datatables" */ "./views/app/ui/datatables/DividedTable")
                            },
                            {
                                path: "scrollable",
                                component: () =>
                                    import ( /* webpackChunkName: "datatables" */ "./views/app/ui/datatables/Scrollable")
                            },
                            {
                                path: "default",
                                component: () =>
                                    import ( /* webpackChunkName: "datatables" */ "./views/app/ui/datatables/Default")
                            }
                        ]
                    },
                    {
                        path: "components",
                        component: () =>
                            import ( /* webpackChunkName : "components" */ "./views/app/ui/components"),
                        redirect: `${adminRoot}/ui/components/alerts`,
                        children: [{
                                path: "alerts",
                                component: () =>
                                    import ( /* webpackChunkName: "components" */ "./views/app/ui/components/Alerts")
                            },
                            {
                                path: "badges",
                                component: () =>
                                    import ( /* webpackChunkName: "components" */ "./views/app/ui/components/Badges")
                            },
                            {
                                path: "buttons",
                                component: () =>
                                    import ( /* webpackChunkName: "components" */ "./views/app/ui/components/Buttons")
                            },
                            {
                                path: "cards",
                                component: () =>
                                    import ( /* webpackChunkName: "components" */ "./views/app/ui/components/Cards")
                            },
                            {
                                path: "carousel",
                                component: () =>
                                    import ( /* webpackChunkName: "components" */ "./views/app/ui/components/Carousel")
                            },
                            {
                                path: "charts",
                                component: () =>
                                    import ( /* webpackChunkName: "components" */ "./views/app/ui/components/Charts")
                            },
                            {
                                path: "collapse",
                                component: () =>
                                    import ( /* webpackChunkName: "components" */ "./views/app/ui/components/Collapse")
                            },
                            {
                                path: "dropdowns",
                                component: () =>
                                    import ( /* webpackChunkName: "components" */ "./views/app/ui/components/Dropdowns")
                            },
                            {
                                path: "editors",
                                component: () =>
                                    import ( /* webpackChunkName: "components" */ "./views/app/ui/components/Editors")
                            },
                            {
                                path: "icons",
                                component: () =>
                                    import ( /* webpackChunkName: "components" */ "./views/app/ui/components/Icons")
                            },
                            {
                                path: "input-groups",
                                component: () =>
                                    import ( /* webpackChunkName: "components" */ "./views/app/ui/components/InputGroups")
                            },
                            {
                                path: "jumbotron",
                                component: () =>
                                    import ( /* webpackChunkName: "components" */ "./views/app/ui/components/Jumbotron")
                            },
                            {
                                path: "maps",
                                component: () =>
                                    import ( /* webpackChunkName: "components" */ "./views/app/ui/components/Maps")
                            },
                            {
                                path: "modal",
                                component: () =>
                                    import ( /* webpackChunkName: "components" */ "./views/app/ui/components/Modal")
                            },
                            {
                                path: "navigation",
                                component: () =>
                                    import ( /* webpackChunkName: "components" */ "./views/app/ui/components/Navigation")
                            },
                            {
                                path: "popover-tooltip",
                                component: () =>
                                    import ( /* webpackChunkName: "components" */ "./views/app/ui/components/PopoverTooltip")
                            },
                            {
                                path: "sortable",
                                component: () =>
                                    import ( /* webpackChunkName: "components" */ "./views/app/ui/components/Sortable")
                            },
                            {
                                path: "tables",
                                component: () =>
                                    import ( /* webpackChunkName: "components" */ "./views/app/ui/components/Tables")
                            }
                        ]
                    }
                ]
            },
            {
                path: "menu",
                component: () =>
                    import ( /* webpackChunkName: "menu" */ "./views/app/menu"),
                redirect: `${adminRoot}/menu/types`,
                children: [{
                        path: "types",
                        component: () =>
                            import ( /* webpackChunkName : "menu-types" */ "./views/app/menu/Types")
                    },
                    {
                        path: "levels",
                        component: () =>
                            import ( /* webpackChunkName : "menu-levels" */ "./views/app/menu/levels"),
                        redirect: `${adminRoot}/menu/levels/third-level-1`,
                        children: [{
                                path: "third-level-1",
                                component: () =>
                                    import ( /* webpackChunkName: "menu-levels" */ "./views/app/menu/levels/Third-level-1")
                            },
                            {
                                path: "third-level-2",
                                component: () =>
                                    import ( /* webpackChunkName: "menu-levels" */ "./views/app/menu/levels/Third-level-2")
                            },
                            {
                                path: "third-level-3",
                                component: () =>
                                    import ( /* webpackChunkName: "menu-levels" */ "./views/app/menu/levels/Third-level-3")
                            }
                        ]
                    }
                ]
            },
            {
                path: "blank-page",
                component: () =>
                    import ( /* webpackChunkName: "blank-page" */ "./views/app/blank-page")
            },
            {
                path: "mega",
                component: () =>    
                    import ( /* webpackChunkName: "New Mega" */ "./views/app/mega/AdminMega")
            }
        ]
    },
    {
        path: "/error",
        component: () =>
            import ( /* webpackChunkName: "error" */ "./views/Error")
    },
    {
        path: "/user",
        component: () =>
            import ( /* webpackChunkName: "user" */ "./views/user"),
        redirect: "/user/login",
        children: [{
                path: "login",
                component: () =>
                    import ( /* webpackChunkName: "user" */ "./views/user/Login")
            },
            {
                path: "register",
                component: () =>
                    import ( /* webpackChunkName: "user" */ "./views/user/Register")
            },
            {
                path: "forgot-password",
                component: () =>
                    import ( /* webpackChunkName: "user" */ "./views/user/ForgotPassword")
            },
            {
                path: "reset-password",
                component: () =>
                    import ( /* webpackChunkName: "user" */ "./views/user/ResetPassword")
            },

        ]
    },
    {
        path: "*",
        component: () =>
            import ( /* webpackChunkName: "error" */ "./views/Error")
    },
    
];

const router = new VueRouter({
    linkActiveClass: "active",
    routes,
    mode: "history"
});

export default router;