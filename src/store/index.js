import Vue from 'vue'
import Vuex from 'vuex'

import app from '../main'
import menu from './modules/menu'
import user from './modules/user'
import chat from './modules/chat'
import todo from './modules/todo'
import survey from './modules/survey'
import getuser from './modules/getuser'
import { setCurrentLanguage } from '../utils'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    balance: 0,
    operation: 'Token',
    currentUser: {},
    role: '',
    percentage: 0,
    megaraffle: 0,
    raffle: false,
    idUser: 0,
    timeLogin: ''
  },
  mutations: {
    changeLang (state, payload) {
      app.$i18n.locale = payload
      setCurrentLanguage(payload);
    },
    addCurrentUser(state, payload){
      state.currentUser = payload
    },
    SET_OPERATION: (state, newValue) => {
      state.operation = newValue
    },
    SET_ROLE: (state, newValue) => {
      state.role = newValue;
    },
    SET_PERCENTAGE : (state, newValue) => {
      state.percentage = newValue;
    },
    SET_MEGARAFFLE : (state, newValue) => {
      state.megaraffle = newValue;
    },
    SET_RAFFLE: (state, newValue) => {
      state.raffle = newValue;
    },
    SET_TIME_LOGIN: (state, newValue) => {
      state.timeLogin = newValue;
    }
    
  },
  actions: {
    setLang ({ commit }, payload) {
      commit('changeLang', payload)
    },
    setCurrentUser(context){
      context.commit('addCurrentUser')
    },
    setOperationAction: ({commit, state}, newValue) => {
      commit('SET_OPERATION', newValue)
      return state.message
    },
    setRoleAction: ({commit, state}, newValue) => {
      commit('SET_ROLE', newValue);
      return state.role;
    },
    setPercentage: ({commit, state}, newValue) => {
      commit('SET_PERCENTAGE', newValue);
      return state.percentage;
    },
    setRaffle: ({commit, state}, newValue) => {
      commit('SET_RAFFLE', newValue);
      return state.raffle;
    },
    setMegaRaffle: ({commit, state}, newValue) => {
      commit('SET_MEGARAFFLE', newValue);
      return state.megaraffle;
    },
    setTimeLogin: ({commit, state}, newValue) => {
      commit('SET_TIME_LOGIN', newValue);
      return state.timeLogin;
    },     
  },
  modules: {
    menu,
    user,
    chat,
    todo,
    survey,
    getuser
  }
})