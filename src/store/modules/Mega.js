import axios from "axios";


import { apiUrl } from '../../constants/config';

const token = localStorage.getItem('token');
axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
const dataUser = JSON.parse(localStorage.getItem('user'));
const headers = {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
  }

class Mega{

    async get(){
        return await axios.get(`${apiUrl}/meraffles`,{},
        {
            headers:headers
        }
        )
        .then(response => {
           return response;
        })
    }

    async newMega(form){
        return await axios.post(`${apiUrl}/meraffle/new`, form, { headers:headers })
        .then( response => {
            return response;
        });
    }

    async saveBetMega(form){
        return await axios.post(`${apiUrl}/megaraffle/bet`, form, { headers:headers })
        .then( response => {
            return response;
        });
    }

    async inactiveBetMega(form){
        return await axios.put(`${apiUrl}/megaraffle/inactive`, form, { headers:headers })
        .then( response => {
            return response;
        });
    }

    async searchMega(id){
        return await axios.get(`${apiUrl}/megaraffle/search`, {
            params:{
                id: id
            }
        }, { headers:headers })
        .then( response => {
            return response;
        });
    }

    
  
}

export default new Mega();
