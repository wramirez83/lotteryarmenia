import axios from "axios";

import { apiUrl } from '../../constants/config';
import closeSession from './closesession';

const token = localStorage.getItem('token');
axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
const headersApi = { 'Access-Control-Allow-Origin': '*','Content-Type': 'application/json', }

class award{

    async getAwardDay(day, end){
      return await axios.get(`${apiUrl}/scrutiny/award`,
      {
        params:{
          dateStart: day,
          endDate: end
        }
      },
      {
        headers:headersApi
      })
      .then(response => {
        return response;
      });
    }

    async deleteAward(id){
      return await axios.delete(`${apiUrl}/scrutiny/award/delete`,
      {
          params:{
            id: id
          }
      },
      {
        headers:headersApi
      })
      .then(response => {
        return response;
      });
    }

    async deleteAwardRaffle(id){
      return await axios.delete(`${apiUrl}/scrutiny/award/deleteRaffle`,
      {
          params:{
            id: id
          }
      },
      {
        headers:headersApi
      })
      .then(response => {
        return response;
      });
    }

    async prepareWh(id){
      return await axios.get(`${apiUrl}/scrutiny/prepareWh`,
      {
          params:{
            id: id
          }
      },
      {
        headers:headersApi
      })
      .then(response => {
        return response;
      });
    }

    async prepareWhRaffle(id){
      return await axios.get(`${apiUrl}/scrutiny/raffle/prepareWh`,
      {
          params:{
            id: id
          }
      },
      {
        headers:headersApi
      })
      .then(response => {
        return response;
      });
    }

    async getAwardDayRaffle(day, end){
      return await axios.get(`${apiUrl}/scrutiny/awardRaffle`,
      {
        params:{
          dateStart: day,
          endDate: end
        }
      },
      {
        headers:headersApi
      })
      .then(response => {
        return response;
      });
    }

}
export default new award();
