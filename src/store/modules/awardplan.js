import axios from "axios";

import router from "../../router";
import { adminRoot } from "../../constants/config";
import { apiUrl } from '../../constants/config';
import closeSession from './closesession';

const token = localStorage.getItem('token');
axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
const headersApi = { 'Access-Control-Allow-Origin': '*','Content-Type': 'application/json', }

class awardplan{

    async get(id){
        return await axios.get(`${apiUrl}/getAwardPlans`,
        {
            params:{
                id: id
            }
        },
        {
            headers:headersApi
        }
        )
        .then(response => {
            closeSession.validate(response.data.status);
            return response;
        })
       
    }
}

export default new awardplan();
