import router from "../../router";

class closeSession{
    validate(msg){
        if(msg == 'Token is Expired' || msg == 'Token is Invalid'){
            localStorage.removeItem('token');
            localStorage.removeItem('user');
            router.push(`/user/login`);
          }
    }

    Inactive(msg){
        if(msg == 'Inactivo'){
            localStorage.removeItem('token');
            localStorage.removeItem('user');
            router.push(`/user/login`);
          }
    }
}

export default new closeSession();