import axios from "axios";

import { apiUrl } from '../../constants/config';
import closeSession from './closesession';

const token = localStorage.getItem('token');
axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
const headersApi = { 'Access-Control-Allow-Origin': '*','Content-Type': 'application/json', }

class closing{

    async dataClosing(id, start, end){
        return await axios.get(`${apiUrl}/closing`,
        {
            params: {
                userId: id,
                startDate: start,
                endDate: end
            }
        },
        {
            headers:headersApi
        }
        )
        .then(response => {
            closeSession.validate(response.data.status);
            return response;
        })
    }
    async getStubForTicket(idTicket){
        return await axios.get(`${apiUrl}/sale/getStubForTicket`,
        {
            params: {
                id: idTicket,
            }
        },
        {
            headers:headersApi
        }
        )
        .then(response => {
            closeSession.validate(response.data.status);
            return response;
        })
    }

}
export default new closing();
