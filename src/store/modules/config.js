import axios from "axios";

import router from "../../router";
import { adminRoot } from "../../constants/config";
import { apiUrl } from '../../constants/config';
import closeSession from './closesession';

const token = localStorage.getItem('token');
axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
const headersApi = { 'Access-Control-Allow-Origin': '*','Content-Type': 'application/json', }

class config{

  async getPermissions(){
    return await axios.get(`${apiUrl}/permissions`,{},
    {
        headers:headersApi
    }
    )
    .then(response => {
      closeSession.validate(response.data.status);
       return response;
    })
    .catch(err => {
      console.log(`Error==>${err}`);
    });
  }
  async getRoles(){
    return await axios.get(`${apiUrl}/getRoles`,{},
    {
        headers:headersApi
    }
    )
    .then(response => {
      closeSession.validate(response.data.status);
       return response;
    })
    .catch(err => {
      console.log(`Error==>${err}`);
    });
  }
}

export default new config();
