const f = new Date();
const  months = new Array ("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
export const todayNumber = f.getDate() + "-" + (f.getMonth() +1) + "-" + f.getFullYear();
export const todayString = f.getDate() + " de " + months[f.getMonth()] + " de " + f.getFullYear();

