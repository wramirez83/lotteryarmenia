import axios from "axios";


import { apiUrl } from '../../constants/config';

const token = localStorage.getItem('token');
axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
const dataUser = JSON.parse(localStorage.getItem('user'));

class getBet{

    async saveBet(nameLottery, pushNumber, client){
        return await axios.post(`${apiUrl}/saveBet`,
        {
            user_id: dataUser.id,
            lotterys: nameLottery,
            numbers: pushNumber,
            client: client
        },
        {
            headers:{
              'Access-Control-Allow-Origin': '*',
              'Content-Type': 'application/json',
            }
        }
        )
        .then(response => {
           return response;
        })
        
    }
  
}

export default new getBet();
