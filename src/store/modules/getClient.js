import axios from "axios";


import { apiUrl } from '../../constants/config';

const token = localStorage.getItem('token');
axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
const dataUser = JSON.parse(localStorage.getItem('user'));

class getClient{

    async get(){
        return await axios.get(`${apiUrl}/getClient`,{
          params:{
            user_id: dataUser.id
          }
        },
        {
            headers:{
              'Access-Control-Allow-Origin': '*',
              'Content-Type': 'application/json',
            }
        }
        )
        .then(response => {
           return response.data;
        })
        .catch(err => {
          console.log('Error 10');
        });
    }
    async getClientAdmin(nameClient){
        return await axios.get(`${apiUrl}/getClient/admin`,{
          params:{
            name: nameClient
          }
        },
        {
            headers:{
              'Access-Control-Allow-Origin': '*',
              'Content-Type': 'application/json',
            }
        }
        )
        .then(response => {
           return response.data;
        })
        .catch(err => {
          console.log('Error 10');
        });
    }

    async newClient(nameClient, phoneClient){
      return await axios.post(`${apiUrl}/newClient`,{
          user_id: dataUser.id,
          name: nameClient,
          phone: phoneClient
        },
        {
          headers:{
            'Access-Control-Allow-Origin': '*',
            'Content-Type': 'application/json',
          }
      })
      .then(response => {
        return response.data;
      })
      .catch(err => {
        console.log('Error 10');
      });
    }

    async updateClient(id, nameClient, phoneClient){
      return await axios.post(`${apiUrl}/updateClient`,{
          id: id,
          name: nameClient,
          phone: phoneClient
        },
        {
          headers:{
            'Access-Control-Allow-Origin': '*',
            'Content-Type': 'application/json',
          }
      })
      .then(response => {
        return response;
      })
      .catch(err => {
        console.log('Error 10');
      });
    }

    async updateClientStatus(id, st){
      return await axios.post(`${apiUrl}/updateClientStatus`,{
          id: id,
          status: st,
        },
        {
          headers:{
            'Access-Control-Allow-Origin': '*',
            'Content-Type': 'application/json',
          }
      })
      .then(response => {
        return response;
      })
      .catch(err => {
        console.log('Error 10');
      });
    }
}

export default new getClient();
