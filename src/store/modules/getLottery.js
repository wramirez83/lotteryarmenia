import axios from "axios";

import router from "../../router";
import { adminRoot } from "../../constants/config";
import { apiUrl } from '../../constants/config';

const token = localStorage.getItem('token');
axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;

class getLottery{

    async lotteryDay(){
        return await axios.get(`${apiUrl}/lotteryDay`,{},
        {
            headers:{
              'Access-Control-Allow-Origin': '*',
              'Content-Type': 'application/json',
            }
        }
        )
        .then(response => {

          if(response.data.status == 'Token is Expired' || response.data.status == 'Token is Invalid'){
            localStorage.removeItem('token');
            localStorage.removeItem('user');
            router.push(`${adminRoot}/user/login`);
          }
           return response.data;
        })
        .catch(err => {
          console.log('Error 10');
        });
    }

    async lotteryDayRaffle(){
      return await axios.get(`${apiUrl}/lotterylistraffle`,{},
      {
          headers:{
            'Access-Control-Allow-Origin': '*',
            'Content-Type': 'application/json',
          }
      }
      )
      .then(response => {

        if(response.data.status == 'Token is Expired' || response.data.status == 'Token is Invalid'){
          localStorage.removeItem('token');
          localStorage.removeItem('user');
          router.push(`${adminRoot}/user/login`);
        }
         return response.data;
      })
      .catch(err => {
        console.log('Error 10');
      });
  }


    async lottery(){
      return await axios.get(`${apiUrl}/lotterylist`,{},
      {
          headers:{
            'Access-Control-Allow-Origin': '*',
            'Content-Type': 'application/json',
          }
      }
      )
      .then(response => {

        if(response.data.status == 'Token is Expired' || response.data.status == 'Token is Invalid'){
          localStorage.removeItem('token');
          localStorage.removeItem('user');
          router.push(`${adminRoot}/user/login`);
        }
         return response.data;
      })
      .catch(err => {
        console.log('Error 10');
      });
  }
  async updateLotteryStatus(lotteryId){
    return await axios.put(`${apiUrl}/updateLotteryStatus`,{
        id:lotteryId
    },
    {
        headers:{
          'Access-Control-Allow-Origin': '*',
          'Content-Type': 'application/json',
        }
    }
    )
    .then(response => {

      if(response.data.status == 'Token is Expired' || response.data.status == 'Token is Invalid'){
        localStorage.removeItem('token');
        localStorage.removeItem('user');
        router.push(`${adminRoot}/user/login`);
      }
       return response.data;
    })
    .catch(err => {
      console.log('Error 10');
    });
}
async saveLottery(data){
  return await axios.post(`${apiUrl}/saveLottery`,{
      data: data
  },
  {
      headers:{
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json',
      }
  }
  )
  .then(response => {

    if(response.data.status == 'Token is Expired' || response.data.status == 'Token is Invalid'){
      localStorage.removeItem('token');
      localStorage.removeItem('user');
      router.push(`${adminRoot}/user/login`);
    }
     return response.data;
  })
  .catch(err => {
    console.log('Error 10');
  });
}

}

export default new getLottery();
