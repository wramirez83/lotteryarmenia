import axios from "axios";


import { apiUrl } from '../../constants/config';
import storage from "./storage";


class getuser{

    async login(document, password){
        localStorage.removeItem('token');
        localStorage.removeItem('user');
        localStorage.removeItem('permission');
        localStorage.clear();
        return await axios.post(`${apiUrl}/login`,
        {
            document: document,
            password: password
        },
        {
           // withCredentials: false, //
            headers:{
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json',
            }
        }
        )
        .then(response => {
            storage.deleteAll();
            localStorage.setItem('token', response.data.token);
            localStorage.setItem('user', JSON.stringify(response.data.getUser));
            localStorage.setItem('permission', JSON.stringify(response.data.permission));
            
            return response;
        })
        .catch(error => {
            return error;
        });
    }
}

export default new getuser();
