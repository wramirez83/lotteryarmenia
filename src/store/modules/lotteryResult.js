import axios from "axios";

import { apiUrl } from '../../constants/config';
import closeSession from './closesession';

const token = localStorage.getItem('token');
axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
const headersApi = { 'Access-Control-Allow-Origin': '*','Content-Type': 'application/json', }

class lotteryResult{

    async setLotteryResult(form){
        return await axios.post(`${apiUrl}/result/add`,
        {
            data: form
        },
        {
            headers:headersApi
        }
        )
        .then(response => {
            closeSession.validate(response.data.status);
            return response;
        })
    }

    async getLotteryResult(ResultDateI){
        return await axios.get(`${apiUrl}/result`,
        {
            params: {
                resultDate: ResultDateI
            }
        },
        {
            timeout: 1,
            headers:headersApi
        }
        )
        .then(response => {
            closeSession.validate(response.data.status);
            return response;
        })
    }
}
export default new lotteryResult();
