import axios from "axios";

import router from "../../router";
import { adminRoot } from "../../constants/config";
import { apiUrl } from '../../constants/config';
import closeSession from './closesession';

const token = localStorage.getItem('token');
axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
const headersApi = { 'Access-Control-Allow-Origin': '*','Content-Type': 'application/json', }

class lotterylimit{

    async getlimit(){
        return await axios.get(`${apiUrl}/limitlotterys`,{},
        {
            headers:headersApi
        }
        )
        .then(response => {
          closeSession.validate(response.data.status);
           return response.data;
        })
        .catch(err => {
          console.log(`Error==>${err}`);
        });
    }

    async updateStatus(id){
      return await axios.put(`${apiUrl}/updatelimit`,
      {
        id: id
      },
      {
        headers:headersApi
      })
      .then(response => {
        closeSession.validate(response.data.status);
        return response.data;
      })
      .catch(err => {
        console.log(`Error==>${err}`);
      })
    }
    async save(data){
      return await axios.post(`${apiUrl}/savelimit`, { data:data }, { headers:headersApi})
      .then(response => {
        return response;
      });
    }

    async saveGlobal(data){
      return await axios.post(`${apiUrl}/savelimitGlobal`, { data:data }, { headers:headersApi})
      .then(response => {
        return response;
      });
    }
    async delete(id){
      return await axios.delete(`${apiUrl}/deletelimit`, { params:{id:id} }, { headers:headersApi})
      .then(response => {
        return response;
      });
    }

}

export default new lotterylimit();
