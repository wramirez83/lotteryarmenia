import axios from "axios";


import { apiUrl } from '../../constants/config';

const token = localStorage.getItem('token');
axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
const dataUser = JSON.parse(localStorage.getItem('user'));

class promotore{

    async get(){
        return await axios.get(`${apiUrl}/getPromotore`,{
          params:{
            user_id: dataUser.id
          }
        },
        {
            headers:{
              'Access-Control-Allow-Origin': '*',
              'Content-Type': 'application/json',
            }
        }
        )
        .then(response => {
           return response.data;
        })
        .catch(err => {
          console.log('Error 10');
        });
    }
}

export default new promotore();
