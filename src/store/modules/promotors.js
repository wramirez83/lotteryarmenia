import axios from "axios";

import { apiUrl } from '../../constants/config';
import closeSession from './closesession';

const token = localStorage.getItem('token');
axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
const headersApi = { 'Access-Control-Allow-Origin': '*','Content-Type': 'application/json', }

class promotors{

    async get(role){
        return await axios.get(`${apiUrl}/getPromotore`,
        {
            params: {
                role: role,
            }
        },
        {
            headers:headersApi
        }
        )
        .then(response => {
            closeSession.validate(response.data.status);
            return response;
        })
       
    }

    async getSeller(document, password){
        return await axios.get(`${apiUrl}/getSeller`,
        {

        },
        {
            headers:headersApi
        }
        )
        .then(response => {
            closeSession.validate(response.data.status);
            return response;
        })
       
    }

    async getUserForRecharge(role){
        return await axios.get(`${apiUrl}/getPromotoreForRechargue`,
        {
            params:{
                role: role
            }
        },
        {
            headers:headersApi
        }
        )
        .then(response => {
            closeSession.validate(response.data.status);
            return response;
        })
       
    }


    async searchUser(role, search){
        return await axios.get(`${apiUrl}/searchUser`,
        {
            params: {
                role: role,
                data: search
            }
        },
        {
            headers:headersApi
        }
        )
        .then(response => {
            closeSession.validate(response.data.status);
            return response;
        })
       
    }

    async changeStatusPromotore(st, id){
        return await axios.put(`${apiUrl}/promotore/chanegestatus`, {
            id: id,
            status: st
        })
        .then(response => {
            return response;
        })
        .catch(error => {

        });
    }

    async getPromotoreAll(){
        return await axios.get(`${apiUrl}/getPromotoreAll`,
        {
           
        },
        {
            headers:headersApi
        }
        )
        .then(response => {
            closeSession.validate(response.data.status);
            return response;
        })
       
    }
}

export default new promotors();
