import axios from "axios";

import { apiUrl } from '../../constants/config';
import closeSession from './closesession';

const token = localStorage.getItem('token');
axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
const headersApi = { 'Access-Control-Allow-Origin': '*','Content-Type': 'application/json', }

class recharge{

    
    async add(who_id, amount){
      return await axios.post(`${apiUrl}/recharge/add`, 
      { 
        who_id:who_id,
        amount: amount
      },
      { 
        headers:headersApi
      })
      .then(response => {
        console.log(response);
        return response;
      });
    }
    
    async del(who_id, amount){
      return await axios.post(`${apiUrl}/recharge/del`, 
      { 
        who_id:who_id,
        amount: amount
      },
      { 
        headers:headersApi
      })
      .then(response => {
        console.log(response);
        return response;
      });
    } 
}

export default new recharge();
