import axios from "axios";

import { apiUrl } from '../../constants/config';
import closeSession from './closesession';

const token = localStorage.getItem('token');
axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
const headersApi = { 'Access-Control-Allow-Origin': '*','Content-Type': 'application/json', }

class results{

    
    async getResult(ResultDateL){
      return await axios.get(`${apiUrl}/result`, 
      { 
        params:{
          resultDate: ResultDateL
        } 
      },
      { 
        headers:headersApi
      })
      .then(response => {
        return response;
      });
    }
    
}

export default new results();
