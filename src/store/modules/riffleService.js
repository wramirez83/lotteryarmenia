import axios from "axios";

import { apiUrl } from '../../constants/config';
import closeSession from './closesession';

const token = localStorage.getItem('token');
axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
const headersApi = { 'Access-Control-Allow-Origin': '*','Content-Type': 'application/json', }

class riffleService{

    async saveRiffle(lotteries, numbers, client){
        return await axios.post(`${apiUrl}/raffle/save`,
        {
            lotteries: lotteries,
            numbers: numbers,
            client: client,
        },
        {
            headers:headersApi
        }
        )
        .then(response => {
            closeSession.validate(response.data.status);
            return response;
        })
       
    }

    async deleteRiffle(id){
        return await axios.delete(`${apiUrl}/raffle/delete`,
        {
            params: {
                id: id
            }
        },
        {
            headers:headersApi
        }
        )
        .then(response => {
            closeSession.validate(response.data.status);
            return response;
        })
       
    }
   
}

export default new riffleService();
