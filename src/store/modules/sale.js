import axios from "axios";

import router from "../../router";
import { adminRoot } from "../../constants/config";
import { apiUrl } from '../../constants/config';
import closeSession from './closesession';

const token = localStorage.getItem('token');
axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
const headersApi = { 'Access-Control-Allow-Origin': '*', 'Content-Type': 'application/json', }

class sale {

    async getSale(dateStart = '', dateEnd = '') {
        return await axios.get(`${apiUrl}/sale/last`, {
                params: {
                    dateStart: dateStart,
                    dateEnd: dateEnd
                }
            }, {
                headers: headersApi
            })
            .then(response => {
                closeSession.validate(response.data.status);
                return response;
            })
            .catch(err => {
                console.log(`Error==>${err}`);
            });
    }
    async getSaleAll(dateStart = '', dateEnd = '') {
        return await axios.get(`${apiUrl}/sale/all`, {
                params: {
                    dateStart: dateStart,
                    dateEnd: dateEnd
                }
            }, {
                headers: headersApi
            })
            .then(response => {
                closeSession.validate(response.data.status);
                return response;
            })
            .catch(err => {
                console.log(`Error==>${err}`);
            });
    }

    async deleteStub(id, lotterysId) {
        return await axios.delete(`${apiUrl}/sale/delete`, {
                params: {
                    id: id,
                    lotterysId: lotterysId
                }
            }, {
                headers: headersApi
            })
            .then(response => {
                return response;
            });
    }

    async searchNumberDate(lottery, date, number) {
        return await axios.get(`${apiUrl}/sale/searchnumberdate`, {
                params: {
                    lottery_id: lottery,
                    date: date,
                    number: number
                }
            }, {
                headers: headersApi
            })
            .then(response => {
                return response;
            });
    }

    async getStubsUsers(star, end, user_id) {
        return await axios.get(`${apiUrl}/sale/getStubsUsers`, {
                params: {
                    star: star,
                    end: end,
                    user_id: user_id
                }
            }, {
                headers: headersApi
            })
            .then(response => {
                return response;
            });
    }

    async getSaleDay(star, end, clientId) {
        return await axios.get(`${apiUrl}/sale/dayofday`, {
                params: {
                    dateStart: star,
                    dateEnd: end,
                    client_id: clientId
                }
            }, {
                headers: headersApi
            })
            .then(response => {
                return response;
            });
    }
    async getSaleRangeDate(start, end, id) {
            return await axios.get(`${apiUrl}/sale/rangedate`, {
                    params: {
                        startDate: start,
                        endDate: end,
                        userId: id
                    }
                }, {
                    headers: headersApi
                })
                .then(response => {
                    return response;
                });
        }
        //getSaleRangeDate

    async getSaleRangeDateSeller(start, end, id) {
        return await axios.get(`${apiUrl}/sale/rangedateseller`, {
                params: {
                    startDate: start,
                    endDate: end,
                    userId: id
                }
            }, {
                headers: headersApi
            })
            .then(response => {
                return response;
            });
    }

    async getSaleRangeDateSonOfSon(start, end, id) {
        return await axios.get(`${apiUrl}/sale/getSaleRangeDateSonOfSon`, {
                params: {
                    startDate: start,
                    endDate: end,
                    userId: id
                }
            }, {
                headers: headersApi
            })
            .then(response => {
                return response;
            });
    }


    async getSaleRangeDateMega(start, end) {
        return await axios.get(`${apiUrl}/megaraffle/close`, {
                params: {
                    startDate: start,
                    endDate: end,
                }
            }, {
                headers: headersApi
            })
            .then(response => {
                return response;
            });
    }
}

export default new sale();