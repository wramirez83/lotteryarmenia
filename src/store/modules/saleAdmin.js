import axios from "axios";

import { apiUrl } from '../../constants/config';
import closeSession from './closesession';

const token = localStorage.getItem('token');
axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
const headersApi = { 'Access-Control-Allow-Origin': '*','Content-Type': 'application/json', }

class saleAdmin{

    async getToday(id){
        return await axios.get(`${apiUrl}/resume/all`,
        {
        },
        {
            headers:headersApi
        }
        )
        .then(response => {
            // closeSession.validate(response.data.status);
            return response;
        })
       
    }

    async todayNet(id){
        return await axios.get(`${apiUrl}/sale/todayNet`,
        {
        },
        {
            headers:headersApi
        }
        )
        .then(response => {
            // closeSession.validate(response.data.status);
            return response;
        })
       
    }
    
}
export default new saleAdmin();
