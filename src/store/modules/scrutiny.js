import axios from "axios";

import { apiUrl } from '../../constants/config';
import closeSession from './closesession';

const token = localStorage.getItem('token');
axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
const headersApi = { 'Access-Control-Allow-Origin': '*','Content-Type': 'application/json', }

class scrutiny{

    async getValues(ResultDate){
        return await axios.get(`${apiUrl}/scrutiny/values`,
        {
            params: {
                resultDate: ResultDate
            }
        },
        {
            timeout: 10000,
            // headers:headersApi
        }
        )
        .then(response => {
            closeSession.validate(response.data.status);
            return response;
        })
    }

    async generateScrutiny(today){
        return await axios.get(`${apiUrl}/scrutiny/generate`,
        {
            params: {
                date: today
            }
        },
        {
            headers:headersApi
        }
        )
        .then(response => {
            closeSession.validate(response.data.status);
            return response;
        })
    }

    async existScrutiny(today){
        return await axios.get(`${apiUrl}/scrutiny/existScrutiny`,
        {
            params: {
                date: today
            }
        },
        {
            headers:headersApi
        }
        )
        .then(response => {
            closeSession.validate(response.data.status);
            return response;
        })
    }

    async getNumbersPlayed(today, lot){
        return await axios.get(`${apiUrl}/scrutiny/getNumbersPlayed`,
        {
            params: {
                date: today,
                lottery_id: lot
            }
        },
        {
            headers:headersApi
        }
        )
        .then(response => {
            closeSession.validate(response.data.status);
            return response;
        })
    }

    async searchticket(id){
        return await axios.get(`${apiUrl}/scrutiny/searchticket`,
        {
            params: {
                id: id
            }
        },
        {
            headers:headersApi
        }
        )
        .then(response => {
            closeSession.validate(response.data.status);
            return response;
        })
    }

    async searchticketRaffle(id){
        return await axios.get(`${apiUrl}/scrutiny/searchticketRaffle`,
        {
            params: {
                id: id
            }
        },
        {
            headers:headersApi
        }
        )
        .then(response => {
            closeSession.validate(response.data.status);
            return response;
        })
    }

    async setScrutinyManual(betId, scrutinyId){
        return await axios.post(`${apiUrl}/scrutiny/setScrutiny/manual`,
        {
            bet_id: betId,
            scrutiny_id: scrutinyId
        },
        {
            headers:headersApi
        }
        )
        .then(response => {
            closeSession.validate(response.data.status);
            return response;
        })
    }

    async setScrutinyManualRaffle(betId, scrutinyId){
        return await axios.post(`${apiUrl}/scrutiny/setScrutiny/manualRaffle`,
        {
            bet_id: betId,
            scrutiny_id: scrutinyId
        },
        {
            headers:headersApi
        }
        )
        .then(response => {
            closeSession.validate(response.data.status);
            return response;
        })
    }

    async deleteScrutiny(date){
        return await axios.delete(`${apiUrl}/scrutiny/delete`,
        {
            params: {
                date: date,
            }
        },
        {
            headers:headersApi
        }).then(response => {
            closeSession.validate(response.data.status);
            return response;
        });
    }

    async getWinner(date, dateEnd){
        return await axios.get(`${apiUrl}/winner`,
        {
            params: {
                date: date,
                dateEnd: dateEnd
            }
        },
        {
            headers:headersApi
        }
        )
        .then(response => {
            closeSession.validate(response.data.status);
            return response;
        })
    }

    async getLotteryDate(dateStart){
        return await axios.get(`${apiUrl}/scrutiny/getLotteryDate`,
        {
            params: {
                dateStart: dateStart
            }
        },
        {
            headers:headersApi
        }
        )
        .then(response => {
            closeSession.validate(response.data.status);
            return response;
        })
    }

    async getStubsNumberPlayed(stubs){
        return await axios.get(`${apiUrl}/scrutiny/getStubsNumberPlayed`,
        {
            params: {
                stubs: stubs
            }
        },
        {
            headers:headersApi
        }
        )
        .then(response => {
            closeSession.validate(response.data.status);
            return response;
        })
    }

    async delteStubAdmin(stub){
        return await axios.delete(`${apiUrl}/sale/deleteAdmin`,
        {
            params: {id: stub}   
        },
        {
            headers:headersApi
        }
        )
        .then(response => {
            closeSession.validate(response.data.status);
            return response;
        })
    }
}
export default new scrutiny();
