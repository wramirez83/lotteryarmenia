import { methodStorage } from "../../constants/config";

class storage{
    set(key, value){
        let method = this.method();
        method.setItem(key, value);
    }

    get(key){
        //get data form local storage
        let method = this.method();
        return method.getItem(key);
        
    }
    deleteAll(){
        for(let i= 0; i < localStorage.length; i++){
            let key = localStorage.key(i);
            localStorage.removeItem(key);
        }
        localStorage.clear();
    }
    method(){
        return methodStorage == 'local' ? localStorage : sessionStorage;
    }
}

export default new storage();