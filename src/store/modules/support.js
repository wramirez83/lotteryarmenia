import axios from "axios";

import { apiUrl } from '../../constants/config';
import closeSession from './closesession';

const token = localStorage.getItem('token');
axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
const headersApi = { 'Access-Control-Allow-Origin': '*','Content-Type': 'application/json', }

class support{

    async sendMessage(msg, fromUser=''){
        return await axios.post(`${apiUrl}/support/sendText`,{
            msg: msg,
            to: fromUser,
            },
        { headers:headersApi })
        .then(response => {
            closeSession.validate(response.data.status);
            return response;
        })

    }
    async getMessage(){
        return await axios.get(`${apiUrl}/support/getMessage`,{},{ headers:headersApi })
        .then(response => {
            closeSession.validate(response.data.status);
            return response;
        })

    }

    async changeStatus(id, newStatus){
        return await axios.put(`${apiUrl}/support/changeStatus`, {
            id: id,
            status: newStatus,
        },
        {
            headers:headersApi
        })
        .then(response => {
            return response;
        })
    }


    async changeStatusView(){
        return await axios.put(`${apiUrl}/support/changeStatusView`, {

        },
        {
            headers:headersApi
        })
        .then(response => {
            return response;
        })
    }

    async sendFile(file){
        const headers = { 'Access-Control-Allow-Origin': '*','Content-Type': 'application/json', 'Content-Type': 'multipart/form-data' };
        return await axios.post(`${apiUrl}/support/sendFile`, file,{
            headers:{
                'Access-Control-Allow-Origin': '*',
                "Content-Type": "multipart/form-data"
            }
        }
        )
        .then(response => {
            closeSession.validate(response.data.status);
            return response;
        })

    }

    async getFile(id, file){
        return await axios.get(`${apiUrl}/support/getFile`,{
            params: {
                id: id,
                file: file
            }
        },
        {
            headers:headersApi
        })
        .then(response => {
            return response;
        })
    }

    async deleteMessage(id){
        return await axios.delete(`${apiUrl}/support/delete`,{ params:{id: id}}, { headers:headersApi })
        .then(response => {
            closeSession.validate(response.data.status);
            return response;
        })

    }

    async deleteMessagesMass(ids){
      return await axios.post(`${apiUrl}/deleteMessagesMass`,{
        id: ids
        },
    { headers:headersApi })
    .then(response => {
        closeSession.validate(response.data.status);
        return response;
    })
    }

    async deleteMessagesAll(){
      return await axios.post(`${apiUrl}/deleteMessagesAll`,{},
    { headers:headersApi })
    .then(response => {
        closeSession.validate(response.data.status);
        return response;
    })
    }
}

export default new support();
