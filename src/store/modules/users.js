import axios from "axios";

import { apiUrl } from '../../constants/config';
import closeSession from './closesession';
import storage from './storage';

//const token = storage.get('token');

// axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
const headersApi = { 'Access-Control-Allow-Origin': '*', 'Content-Type': 'application/json', }

class users {

    async newUser(data) {
        let token = storage.get('token');
        axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
        return await axios.post(`${apiUrl}/registerUser`, {
                data: data,
            }, {
                headers: headersApi
            })
            .then(response => {
                closeSession.validate(response.data.status);
                return response;
            })

    }
    async getDataUser() {
        let token = storage.get('token');
        axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
        return axios.post(`${apiUrl}/user`, {}, {
            headers: headersApi
        }).then(response => {
            closeSession.Inactive(response.data.user.status)
            closeSession.validate(response.data.status);
            return response;

        })
    }

    async refresh() {
        let token = storage.get('token');
        axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
        return axios.get(`${apiUrl}/refresh`, {}, {
            headers: headersApi
        }).then(response => {
            closeSession.validate(response.data.status);
            localStorage.setItem('token', response.data);
            return response;

        })
    }

    async getUsers(role) {
        let token = storage.get('token');
        axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
        return axios.get(`${apiUrl}/getUsers`, {
            params: {
                role: role
            }
        }, {
            headers: headersApi
        }).then(response => {
            closeSession.validate(response.data.status);
            return response;

        })
    }

    async validateDocument(document){
        let token = storage.get('token');
        axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;

        return axios.get(`${apiUrl}/user/validatedocument`, {
            params: {
                document: document
            }
        }, {
            headers: headersApi
        }).then(response => {
            closeSession.validate(response.data.status);
            return response;

        })

    }

    async changePassword(lastPassword, password){
        let token = storage.get('token');
        axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;

        return axios.put(`${apiUrl}/user/updatePassword`, {
         
                beforePassword: lastPassword,
                password: password
            
        }, {
            headers: headersApi
        }).then(response => {
            closeSession.validate(response.data.status);
            return response;

        })

    }
}

export default new users();