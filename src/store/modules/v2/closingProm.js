import axios from "axios";

import router from "../../../router";
import { adminRoot } from "../../../constants/config";
import { apiUrl } from '../../../constants/config';
import closeSession from './../closesession';

const token = localStorage.getItem('token');
axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
const headersApi = { 'Access-Control-Allow-Origin': '*', 'Content-Type': 'application/json', }

class closingProm {

    
    async getSaleRangeDate(start, end, id) {
            return await axios.get(`${apiUrl}/saleProm`, {
                    params: {
                        startDate: start,
                        endDate: end,
                        userId: id
                    }
                }, {
                    headers: headersApi
                })
                .then(response => {
                    return response;
                });
        }
       

    
}

export default new closingProm();